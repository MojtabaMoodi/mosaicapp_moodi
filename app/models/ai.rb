class Ai < ApplicationRecord
  validates_presence_of :Project_Building_Identifier, :Project_Geographic_District, :Project_School_Name, :Project_Type,
                        :Project_Description, :Project_Phase_Name, :Project_Status_Name, :Project_Phase_Actual_Start_Date,
                        :Project_Phase_Planned_End_Date, :Project_Budget_Amount,
                        :Final_Estimate_of_Actual_Costs_Through_End_of_Phase_Amount, :Total_Phase_Actual_Spending_Amount,
                        :DSF_Number
end
