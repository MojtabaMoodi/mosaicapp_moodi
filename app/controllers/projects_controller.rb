class ProjectsController < ApplicationController
  before_action :find_ai, only: [:similar, :update] 
  # GET /search
  def search

    @ais = Ai.where.not(Project_Status_Name: 'PNS').where(search_params).paginate(page: params[:page], per_page: 30)
    json_response(@ais)
    # if params['Project_School_Name']
    #   @ais = Ai.where.not(Project_Status_Name: 'PNS').where(Project_School_Name: params['Project_School_Name']).paginate(
    #     page: params[:page], per_page: 30
    #   )
    # elsif params['Project_Description']
    #   @ais = Ai.where.not(Project_Status_Name: 'PNS').where(Project_Description: params['Project_Description']).paginate(
    #     page: params[:page], per_page: 30
    #   )
    # end
    # json_response(@ais)
  end

  # PUT /ais/:id
  def update
    update_params = ais_params
    update_params[:Total_Phase_Actual_Spending_Amount] = ais_params[:Total_Phase_Actual_Spending_Amount].to_f
    @ai.update!(update_params)
    head :no_content
  end

  def similar
    pgd = @ai.Project_Geographic_District
    total_value = @ai.Total_Phase_Actual_Spending_Amount

    @ais = Ai.where(Project_Geographic_District: pgd)
    @result = @ais.where("Total_Phase_Actual_Spending_Amount > ? AND Total_Phase_Actual_Spending_Amount < ?" , total_value/2, total_value*2)
    json_response(@result)
  end

  private

  def ais_params
    # whitelist params
    params.permit(:id, :Project_Phase_Actual_Start_Date, :Total_Phase_Actual_Spending_Amount)
  end

  def search_params
    params.permit(:Project_Phase_Actual_Start_Date, :Project_Phase_Actual_End_Date, :Project_Phase_Actual_End_Date,
                  :Project_Budget_Amount, :Final_Estimate_of_Actual_Costs_Through_End_of_Phase_Amount, :Total_Phase_Actual_Spending_Amount)
  end

  def find_ai
    @ai = Ai.find(params[:id])
  end
end
