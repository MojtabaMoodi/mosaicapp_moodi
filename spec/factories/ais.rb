FactoryBot.define do
  factory :ai do
    Project_Geographic_District { Faker::Number.number(digits: 10) }
    Project_Building_Identifier { Faker::Lorem.word }
    Project_School_Name { Faker::Lorem.word }
    Project_Type { Faker::Lorem.word }
    Project_Description { Faker::Lorem.word }
    Project_Phase_Name { Faker::Lorem.word }
    Project_Status_Name { Faker::Lorem.word }
    Project_Phase_Actual_Start_Date { Faker::Lorem.word }
    Project_Phase_Planned_End_Date { Faker::Lorem.word }
    Project_Phase_Actual_End_Date { Faker::Lorem.word }
    Project_Budget_Amount { Faker::Lorem.word }
    Final_Estimate_of_Actual_Costs_Through_End_of_Phase_Amount { Faker::Number.decimal(l_digits: 5, r_digits: 1) }
    Total_Phase_Actual_Spending_Amount { Faker::Number.decimal(l_digits: 4, r_digits: 1) }
    DSF_Number { Faker::Lorem.word }
  end
end
