require 'rails_helper'

RSpec.describe Ai, type: :model do
  describe 'Validations' do
    it { should validate_presence_of(:Project_Geographic_District) }
    it { should validate_presence_of(:Project_Building_Identifier) }
    it { should validate_presence_of(:Project_School_Name) }
    it { should validate_presence_of(:Project_Type) }
    it { should validate_presence_of(:Project_Description) }
    it { should validate_presence_of(:Project_Phase_Name) }
    it { should validate_presence_of(:Project_Status_Name) }
    it { should validate_presence_of(:Project_Phase_Actual_Start_Date) }
    it { should validate_presence_of(:Project_Budget_Amount) }
    it { should validate_presence_of(:Final_Estimate_of_Actual_Costs_Through_End_of_Phase_Amount) }
    it { should validate_presence_of(:Total_Phase_Actual_Spending_Amount) }
    it { should validate_presence_of(:DSF_Number) }
  end
end
