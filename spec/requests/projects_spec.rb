require 'rails_helper'

RSpec.describe 'Ais API', type: :request do
  let!(:ais) { create_list(:ai, 10) }
  # let(:Project_School_Name) { ais.first.Project_School_Name }

  # Test suite for GET /search
  describe 'GET /search' do
    # valid payload
    let(:valid_attribute) { { 'Project_School_Name': ais.first.Project_School_Name } }

    context 'when the record exists' do
      before { get '/search', params: valid_attribute }

      it 'returns the record' do
        expect(json).not_to be_empty
        expect(json[0]['Project_School_Name']).to eq(ais.first.Project_School_Name)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end
  end
end
