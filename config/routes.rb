Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get '/ais/:id/similar', to: 'projects#similar'
  put '/ais/:id', to: 'projects#update'
  get '/search', to: 'projects#search'
end
