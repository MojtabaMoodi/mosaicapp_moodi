# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require 'json'

json_file = File.read(File.join(Rails.root, "AI Dataset.json"))
json_data = JSON.parse(json_file)

# data is going to get inserted to the database.
data = []

json_data.each do |item|
  obj = {
    "Project_Geographic_District": item["Project Geographic District"],
    "Project_Building_Identifier": item["Project Building Identifier"],
    "Project_School_Name": item["Project School Name"],
    "Project_Type": item["Project Type"],
    "Project_Description": item["Project Description"],
    "Project_Phase_Name": item["Project Phase Name"],
    "Project_Status_Name": item["Project Status Name"],
    "Project_Phase_Actual_Start_Date": item["Project Phase Actual Start Date"],
    "Project_Phase_Planned_End_Date": item["Project Phase Planned End Date"],
    "Project_Phase_Actual_End_Date": item["Project Phase Actual End Date"],
    "Project_Budget_Amount": item["Project Budget Amount"],
    "Final_Estimate_of_Actual_Costs_Through_End_of_Phase_Amount": item["Final Estimate of Actual Costs Through End of Phase Amount"],
    "Total_Phase_Actual_Spending_Amount": item["Total Phase Actual Spending Amount"],
    "DSF_Number": item["DSF Number(s)"]
  }

  data << obj
end
Ai.create!(data)